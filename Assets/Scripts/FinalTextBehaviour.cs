﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Add to an UI Text component to show the match winner.
/// It checks the winner name from the GameManager static class.
/// </summary>
public class FinalTextBehaviour : MonoBehaviour
{
    /// <summary>
    /// Assign match winner name to the text field/component
    /// </summary>
	void Start ()
    {
        GetComponent<Text>().text = GameManager.lastWinnerName + " Won the match!";
	}
	
}
