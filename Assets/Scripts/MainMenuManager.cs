﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// MainMenu managing.
/// </summary>
public class MainMenuManager : MonoBehaviour
{
    
    public void BackMainMenu ()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void NewGame ()
    {
        SceneManager.LoadScene("Game");
    }

    public void QuitGame ()
    {
        Application.Quit();
    }
}
