﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic data for attacking(insult) and defending(the right answer) sentence.
/// </summary>
[System.Serializable]
public class InsultAndAnswer
{
    public string insult;
    public string answer;
}
