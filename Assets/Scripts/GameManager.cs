﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// For game managing purposes
/// </summary>
public static class GameManager
{
    /// <summary>
    /// For inter scenes saving winner name purposes
    /// </summary>
    public static string lastWinnerName;
}
