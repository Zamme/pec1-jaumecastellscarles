﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Basic human player behaviour. It's major part inherits from PlayerBehaviour general abstract class.
/// </summary>
public class HumanBehaviour : PlayerBehaviour
{
    /// <summary>
    /// For sentences buttons instantiation.
    /// </summary>
    public GameObject sentenceButtonPrefab;

    /// <summary>
    /// For saving insults from json file.
    /// </summary>
    private GameObject[] insults;
    /// <summary>
    /// For saving answers from json file.
    /// </summary>
    private GameObject[] answers;

    /// <summary>
    /// The transform reference for the sentences parent content object.
    /// </summary>
    private Transform contentSentences;

    /// <summary>
    /// Fill player dialog text with answer [i] sentence, report it to the match manager and 
    /// show the player dialog.
    /// </summary>
    /// <param name="iAnswer">Index of answers array.</param>
    private void ClickAnswer (int iAnswer)
    {
        SaySomething(iAnswer);
        ShowAnswers(false);
    }

    /// <summary>
    /// Fill player dialog text with insult [i] sentence, report it to the match manager and 
    /// show the player dialog.
    /// </summary>
    /// <param name="iInsult">Index of insults array.</param>
    private void ClickInsult(int iInsult)
    {
        SaySomething(iInsult);
        ShowInsults(false);
    }

    /// <summary>
    /// Manage the player depending on its behaviour.
    /// </summary>
    protected override void CheckPlayerBehaviourState()
    {
        switch (playerBehaviourState)
        {
            case PlayerBehaviourState.Idle:
                Animate(false);
                break;
            case PlayerBehaviourState.Defending:
                Animate(false);
                CheckContentSentences();
                ShowSentences(false);
                break;
            case PlayerBehaviourState.Attacking:
                Animate(false);
                CheckContentSentences();
                ShowSentences(true);
                break;
            case PlayerBehaviourState.Losing:
                Animate(true);
                break;
            case PlayerBehaviourState.Winning:
                Animate(true);
                break;
        }
    }

    /// <summary>
    /// Create buttons for human interface (insults and answers)
    /// </summary>
    void CreateSentences ()
    {
        //Debug.Log("Creating sentences...");
        insults = new GameObject[matchManager.currentMatchDictionary.Length];
        answers = new GameObject[matchManager.currentMatchDictionary.Length];

        for (int sentenceIndex = 0; sentenceIndex < matchManager.currentMatchDictionary.Length; sentenceIndex++)
        {
            int iSentence = sentenceIndex; // For listeners: Compulsory because UI buttons listeners uses reference pass

            /// Instantiate button with contentSentences as parent
            insults[sentenceIndex] = GameObject.Instantiate(sentenceButtonPrefab, contentSentences);
            /// Assign sentence to text field/component 
            insults[sentenceIndex].GetComponentInChildren<Text>().text = matchManager.currentMatchDictionary[sentenceIndex].insult; //Posible error in some platforms
            /// Change button gameobject name for clearness purposes
            insults[sentenceIndex].name = "InsultButton" + sentenceIndex.ToString();
            /// Add listener with the sentence index
            insults[sentenceIndex].GetComponent<Button>().onClick.AddListener(delegate { ClickInsult(iSentence); });
            /// Reset scale to avoid parents transform inheritance.
            insults[sentenceIndex].transform.localScale = Vector3.one;

            answers[sentenceIndex] = GameObject.Instantiate(sentenceButtonPrefab, contentSentences);
            answers[sentenceIndex].GetComponentInChildren<Text>().text = matchManager.currentMatchDictionary[sentenceIndex].answer; //Posible error in some platforms
            answers[sentenceIndex].name = "AnswerButton" + sentenceIndex.ToString();
            answers[sentenceIndex].GetComponent<Button>().onClick.AddListener(delegate { ClickAnswer(iSentence); });
            answers[sentenceIndex].transform.localScale = Vector3.one;
        }

        //Debug.Log("Sentences created: Answers=" + answers.Length.ToString() + " Insults=" + insults.Length.ToString());
    }

    /// <summary>
    /// For checking sentences integrity. For multisource on various json files.
    /// </summary>
    void CheckContentSentences ()
    {
        if (!contentSentences)
        {
            GetContentSentences();
        }
    }

    /// <summary>
    /// Save the sentences parent reference and create the sentences (children of)
    /// </summary>
    void GetContentSentences()
    {
        contentSentences = GameObject.Find("ContentSentences").transform;

        CreateSentences();
    }

    /// <summary>
    /// Show the answers buttons.
    /// </summary>
    /// <param name="enable">Show them?</param>
    void ShowAnswers (bool enable)
    {
        //Debug.Log("answers length: " + answers.Length.ToString());
        foreach(GameObject ansGO in answers)
        {
            ansGO.SetActive(enable);
        }
    }

    /// <summary>
    /// Show the insults buttons.
    /// </summary>
    /// <param name="enable">Show them?</param>
    void ShowInsults (bool enable)
    {
        foreach (GameObject insGO in insults)
        {
            insGO.SetActive(enable);
        }
    }

    /// <summary>
    /// Toogle to show insults/answers.
    /// </summary>
    /// <param name="insults">Show insults?(true) - Show answers?(false)</param>
    void ShowSentences (bool insults)
    {
        //Debug.Log("Showing sentences insults:" + insults);
        ShowInsults(insults);
        ShowAnswers(!insults);
    }

}
