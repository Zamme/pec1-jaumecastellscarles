﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// NPC basic behaviour.
/// The major part of its behaviour inherits from PlayerBehaviour general abstract class.
/// </summary>
public class AIBehaviour : PlayerBehaviour
{
    /// <summary>
    /// AI behaviour depending on its behaviour state.
    /// </summary>
    protected override void CheckPlayerBehaviourState()
    {
        switch (playerBehaviourState)
        {
            case PlayerBehaviourState.Idle:
                Animate(false);
                break;
            case PlayerBehaviourState.Defending:
                SaySomething(GetRandomSentence());
                Animate(true);
                break;
            case PlayerBehaviourState.Attacking:
                SaySomething(GetRandomSentence());
                Animate(true);
                break;
            case PlayerBehaviourState.Losing:
                break;
            case PlayerBehaviourState.Winning:
                break;
        }
    }

    /// <summary>
    /// Get a random number of the sentences array.
    /// It's used for both (insult and answer).
    /// </summary>
    /// <returns>Return an integer between 0 and sentences max length.</returns>
    private int GetRandomSentence ()
    {
        return Random.Range(0, matchManager.currentMatchDictionary.Length);
    }

}
