﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class PlayerBehaviour : MonoBehaviour
{
    // for the player state machine
    /// <summary>
    /// For number of won rounds state.
    /// </summary>
    public enum PlayerMatchState
    {
        ZeroWins,
        OneWins,
        TwoWins,
        Winner
    }
    /// - 
    /// - Losing 
    /// - Winning 
    /// - Attacking 
    /// - Defending 

    /// <summary>
    /// For managing the player behaviour
    /// </summary>
    public enum PlayerBehaviourState
    {
        /// <summary>When it's waiting for an insult or and answer.</summary>
        Idle,
        /// <summary>When it's losing a round.</summary>
        Losing,
        /// <summary>When it's winning a round.</summary>
        Winning,
        /// <summary>When it's insulting</summary>
        Attacking,
        /// <summary>When it's answering an insult.</summary>
        Defending
    }

    /// <summary>
    /// Current player match state.
    /// </summary>
    public PlayerMatchState playerMatchState;
    /// <summary>
    /// Current player behavior state.
    /// </summary>
    public PlayerBehaviourState playerBehaviourState;

    /// <summary>
    /// For animation/motion purposes.
    /// </summary>
    public Transform[] positions;

    /// <summary>
    /// Time between rounds.
    /// </summary>
    public float roundPauseTime = 3.0f;

    /// <summary>
    /// Self animator ref.
    /// </summary>
    protected Animator animator;

    /// <summary>
    /// Self dialog text ref.
    /// </summary>
    protected Text dialogText;

    /// <summary>
    /// General match manager ref.
    /// </summary>
    protected MatchManager matchManager;


	/// <summary>
    /// Save its dialog text component reference and initiate player state.  
    /// </summary>
	public virtual void Awake ()
    {
        GetDialogText();
        InitializePlayerState();
	}

    /// <summary>
    /// Play/Stop the speking animation
    /// </summary>
    /// <param name="anim">Speak!</param>
    protected void Animate (bool anim)
    {
        if (!animator)
        {
            GetAnimator();
        }

        animator.SetBool("speaking", anim);
    }

    /// <summary>
    /// Abstract class from wich derives NPCs and Human round behaviour
    /// </summary>
    protected abstract void CheckPlayerBehaviourState();

    /// <summary>
    /// Player match state machine. It manages the behaviour based on the rounds results.
    /// </summary>
    protected void CheckPlayerMatchState ()
    {
        switch (playerMatchState)
        {
            case PlayerMatchState.ZeroWins:
                break;
            case PlayerMatchState.OneWins:
                break;
            case PlayerMatchState.TwoWins:
                break;
            case PlayerMatchState.Winner:
                Debug.Log("You Win!!!");
                matchManager.FinishMatch(gameObject.tag);
                break;
        }
    }

    /// <summary>
    /// Coroutine that reports rounds won, pause a time, hide the dialog, and reset the default initial players round state.
    /// </summary>
    /// <param name="numState">To set current match state</param>
    /// <returns></returns>
    protected IEnumerator EndRoundTimePause(int numState)
    {
        SetPlayerMatchState(numState);
        if (playerMatchState != PlayerMatchState.Winner)
        {
            yield return new WaitForSeconds(roundPauseTime);
            ShowDialogText(false);

            if (playerBehaviourState == PlayerBehaviourState.Winning)
            {
                SetPlayerBehaviourState(PlayerBehaviourState.Attacking);
            }
            else if (playerBehaviourState == PlayerBehaviourState.Losing)
            {
                SetPlayerBehaviourState(PlayerBehaviourState.Idle);
            }
        }
    }

    /// <summary>
    /// Set the player animator component reference to animator property.
    /// </summary>
    private void GetAnimator ()
    {
        animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Set text component of dialog text reference to the dialogText property.
    /// </summary>
    private void GetDialogText ()
    {
        // Ugly way! Fix it!
        dialogText = transform.parent.GetChild(0).GetChild(0).GetComponent<Text>();
        dialogText.text = " ";
    }

    /// <summary>
    /// Reset player stats and behavior.
    /// </summary>
    public void InitializePlayerState ()
    {
        playerMatchState = PlayerMatchState.ZeroWins;
        playerBehaviourState = PlayerBehaviourState.Idle;
    }

    /// <summary>
    /// Set player behavior to losing and set match state to 0.
    /// </summary>
    protected void LoseRound ()
    {
        // If not consecutive style:
        //int numState = StateToInt(playerMatchState);
        //numState--;
        //playerMatchState = StateToEnum(numState);

        // Consecutive style:
        SetPlayerBehaviourState(PlayerBehaviourState.Losing);
        StartCoroutine(EndRoundTimePause(0));
    }

    /// <summary>
    /// Move player to Init_poses 0,1,2. Work in progress :-(
    /// </summary>
    protected void Motion ()
    {
        // Pending to add motion
    }

    /// <summary>
    /// Increase/Decrease match state.
    /// </summary>
    /// <param name="win">Win round?</param>
    public void NotifyRoundResult(bool win)
    {
        if (win)
        {
            WinRound();
        }
        else
        {
            LoseRound();
        }
    }

    /// <summary>
    /// Fill player dialog text with insult/answer [i] sentence and report it to the match manager. 
    /// </summary>
    /// <param name="iSentence">insult/answer index</param>
    protected void SaySomething(int iSentence)
    {
        if (playerBehaviourState == PlayerBehaviourState.Attacking)
        {
            dialogText.text = matchManager.currentMatchDictionary[iSentence].insult;
        }
        else
        {
            dialogText.text = matchManager.currentMatchDictionary[iSentence].answer;
        }


        matchManager.PlayerSaySomething(transform, iSentence);
    }

    protected void SaySomething (string sentence)
    {
        // For testing purposes
        dialogText.text = sentence;
    }

    /// <summary>
    /// Set match manager reference from other source.
    /// </summary>
    /// <param name="mm">match manager reference</param>
    public void SetMatchManager (MatchManager mm)
    {
        matchManager = mm;
    }

    /// <summary>
    /// Sets the player behaviour state and checks for play it.
    /// </summary>
    /// <param name="state">player behavior state</param>
    public void SetPlayerBehaviourState (PlayerBehaviourState state)
    {
        playerBehaviourState = state;
        CheckPlayerBehaviourState();
    }

    /// <summary>
    /// Set the player match state and checks for play it.
    /// </summary>
    /// <param name="numState">player match state</param>
    public void SetPlayerMatchState (int numState)
    {
        playerMatchState = StateToEnum(numState);
        CheckPlayerMatchState();
    }

    /// <summary>
    /// Shows player dialog.
    /// </summary>
    /// <param name="enable">Show it?</param>
    public void ShowDialogText (bool enable)
    {
        dialogText.gameObject.SetActive(enable);
    }

    /// <summary>
    /// Convert integer to player match state.
    /// </summary>
    /// <param name="state">Match state as integer</param>
    /// <returns>Match state as PlayerMatchState</returns>
    public PlayerMatchState StateToEnum (int state)
    {
        return (PlayerMatchState)state;
    }

    /// <summary>
    /// Convert player match state to integer.
    /// </summary>
    /// <param name="state">Match state as PlayerMatchState</param>
    /// <returns>PlayerMatchState as integer</returns>
    public int StateToInt (PlayerMatchState state)
    {
        return (int)state;
    }

    /// <summary>
    /// Increase player match state, set the player behaviour to winning, and set the match state (playing his state machine)
    /// </summary>
    protected void WinRound ()
    {
        int iMatchState = StateToInt(playerMatchState);
        iMatchState++;
        SetPlayerBehaviourState(PlayerBehaviourState.Winning);

        StartCoroutine(EndRoundTimePause(iMatchState));

    }

}
