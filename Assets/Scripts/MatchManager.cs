﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Manage the new battle.
/// </summary>
public class MatchManager : MonoBehaviour
{
    /// <summary>
    /// For managing the match state.
    /// </summary>
    public enum MatchState
    {
        /// <summary>
        /// Loading all necessary to create a new match
        /// </summary>
        Preparing,
        /// <summary>
        /// Prepare all necessary to start the match
        /// </summary>
        Start,
        /// <summary>
        /// Player1 has the turn
        /// </summary>
        Player1Turn,
        /// <summary>
        /// Player2 has the turn
        /// </summary>
        Player2Turn,
        /// <summary>
        /// The match is over
        /// </summary>
        Finish
    }

    /// <summary>
    /// Current match state.
    /// </summary>
    public MatchState matchState;

    /// <summary>
    /// Pirates insult json file.
    /// </summary>
    public string piratesJsonFilePath = "MonkeyIslandPiratesInsults.json";
    //public string swordMasterJsonFilePath = "MonkeyIslandSwordMasterInsults.json";

    /// <summary>
    /// Current background for the battle.
    /// </summary>
    public Sprite battleBackground;

    /// <summary>
    /// The players behaviours. More than 2 players option in a future version? 
    /// </summary>
    public PlayerBehaviour[] playersBehaviours;

    /// <summary>
    /// The current insult/answer dictionary loaded from a json file.
    /// </summary>
    public InsultAndAnswer[] currentMatchDictionary;

    /// <summary>
    /// Pause time in seconds between turns.
    /// </summary>
    public float turnPauseTime = 3.0f;

    /// <summary>
    /// Last sentence index. If -1 then no round started.
    /// </summary>
    int iLastSentenceUsed = -1;

    /// <summary>
    /// Round player winner tag.
    /// </summary>
    string roundWinnerPlayer;

    /// <summary>
    /// Reference to round result UI text component.
    /// </summary>
    Text roundResultText;

    /// <summary>
    /// In game start button component reference.
    /// </summary>
    Button startButton;

    /// <summary>
    /// Players statistics UI texts components.
    /// </summary>
    Text[] playersStatsTexts;

    /// <summary>
    /// Fill the current insults/answers dictionary. Get statistics UI texts refs. Sets match state to preparing for initialization.
    /// </summary>
    void Start ()
    {
        FillDictionary(piratesJsonFilePath);
        GetStatsTexts();
        SetMatchState(MatchState.Preparing);
    }

    /// <summary>
    /// For reset the match.
    /// </summary>
    public void StartMatch ()
    {
        iLastSentenceUsed = -1;
        EnableStartButton(false); // True if we want to reset the match in game.
        LoadMatchBackground();

        if (GetPlayersBehaviours() < 2) // 2 players min.
        {
            Debug.LogError("Number of players less than 2!. Aborting.");
        }
        else
        {
            foreach (PlayerBehaviour pb in playersBehaviours)
            {
                pb.InitializePlayerState();
            }
            UpdateStatsText();
            SetMatchState(MatchState.Start);
        }
    }

    /// <summary>
    /// Back to main menu from everywhere.
    /// </summary>
    public void BackToMainMenu ()
    {
        SceneManager.LoadScene("MainMenu");
    }

    /// <summary>
    /// Match behaviour.
    /// </summary>
    void CheckMatchState ()
    {
        switch (matchState)
        {
            /// Used to preinit some properties.
            case MatchState.Preparing:
                //iLastSentenceUsed = -1;
                break;
            /// Choose who starts the match randomly.
            case MatchState.Start:
                RandomizePlayer();
                break;
            /// Assign attacking state to the player who has the turn and the idle state to the other player.
            case MatchState.Player1Turn:
                ShowRoundText(false);
                foreach (PlayerBehaviour pb in playersBehaviours)
                {
                    if (pb.tag == "Player1")
                    {
                        pb.SetPlayerBehaviourState(PlayerBehaviour.PlayerBehaviourState.Attacking);
                    }
                    else
                    {
                        pb.SetPlayerBehaviourState(PlayerBehaviour.PlayerBehaviourState.Idle);
                    }
                }
                break;
            case MatchState.Player2Turn:
                ShowRoundText(false);
                foreach (PlayerBehaviour pb in playersBehaviours)
                {
                    if (pb.tag == "Player2")
                    {
                        pb.SetPlayerBehaviourState(PlayerBehaviour.PlayerBehaviourState.Attacking);
                    }
                    else
                    {
                        pb.SetPlayerBehaviourState(PlayerBehaviour.PlayerBehaviourState.Idle);
                    }
                }
                break;
            /// When a player gets 3 points.
            case MatchState.Finish:
                // if no final scene added. All in one game scene.
                //EnableStartButton(true);
                StartCoroutine(FinishedMatch());
                break;
        }

    }

    /// <summary>
    /// Is the answer correct?
    /// </summary>
    /// <param name="iSentence">Index of the answer</param>
    /// <returns>Insult and answer have the same index?</returns>
    bool CheckAnswerResult (int iSentence)
    {
        return (iLastSentenceUsed == iSentence);
    }

    /// <summary>
    /// A pause before checking who wins the round. Set it and notify to players.
    /// </summary>
    /// <param name="iSentence">Index of the answer</param>
    /// <returns>Pause of 2 seconds</returns>
    IEnumerator CheckRoundResult (int iSentence)
    {
        yield return new WaitForSeconds(2.0f);

        if (matchState == MatchState.Player1Turn)
        {
            if (CheckAnswerResult(iSentence))
            {
                roundWinnerPlayer = "Player2";
            }
            else
            {
                roundWinnerPlayer = "Player1";
            }
        }
        else
        {
            if (CheckAnswerResult(iSentence))
            {
                roundWinnerPlayer = "Player1";
            }
            else
            {
                roundWinnerPlayer = "Player2";
            }
        }

        SetRoundText(roundWinnerPlayer + " wins!");

        foreach (PlayerBehaviour playerBehaviour in playersBehaviours)
        {
            playerBehaviour.NotifyRoundResult(playerBehaviour.tag == roundWinnerPlayer);
        }

        if (matchState != MatchState.Finish)
        {
            matchState = (roundWinnerPlayer == "Player1") ? MatchState.Player1Turn : MatchState.Player2Turn;
            StartCoroutine(EndRoundPauseSeconds(turnPauseTime));
        }

        iLastSentenceUsed = -1;
    }

    /// <summary>
    /// Enable in game start button.
    /// </summary>
    /// <param name="enable">Enable?</param>
    void EnableStartButton (bool enable)
    {
        if (!startButton)
        {
            GetStartButton();
        }

        startButton.interactable = enable;
    }

    /// <summary>
    /// Update stats. Show round winner text and hide it after a pause.
    /// </summary>
    /// <param name="seconds">Secons of pause showing the round winner</param>
    /// <returns>Seconds pause</returns>
    IEnumerator EndRoundPauseSeconds(float seconds)
    {
        UpdateStatsText();
        ShowRoundText(true);
        yield return new WaitForSeconds(seconds);
        ShowRoundText(false);
    }

    /// <summary>
    /// Pause between turns and toogle turn. The toogle could be done better ;-)
    /// </summary>
    /// <param name="seconds">Seconds of pause</param>
    /// <param name="tag">Winner player tag</param>
    /// <returns></returns>
    IEnumerator EndTurnPauseSeconds(float seconds, string tag)
    {
        yield return new WaitForSeconds(seconds);
        foreach (PlayerBehaviour pb in playersBehaviours)
        {
            if (pb.tag == tag)
            {
                pb.SetPlayerBehaviourState(PlayerBehaviour.PlayerBehaviourState.Idle);
            }
            else
            {
                pb.SetPlayerBehaviourState(PlayerBehaviour.PlayerBehaviourState.Defending);
            }
        }
    }

    /// <summary>
    /// Set player stats UI texts refs.
    /// </summary>
    void GetStatsTexts ()
    {
        playersStatsTexts = new Text[2];
        playersStatsTexts[0] = GameObject.Find("Player1RoundsText").GetComponent<Text>();
        playersStatsTexts[1] = GameObject.Find("Player2RoundsText").GetComponent<Text>();
    }

    /// <summary>
    /// Set start button UI component reference.
    /// </summary>
    void GetStartButton ()
    {
        startButton = GameObject.Find("StartButton").GetComponent<Button>();
    }

    /// <summary>
    /// Read json file and fill current dictionary.
    /// </summary>
    /// <param name="jsonFilePath">Insult/answer dictionary json file path.</param>
    void FillDictionary (string jsonFilePath)
    {
        string filePath = Path.Combine(Application.streamingAssetsPath, jsonFilePath);
        Debug.Log(filePath);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            Debug.Log(dataAsJson);
            ReadInsultArray(dataAsJson);
        }
        else
        {
            Debug.Log("Json file not found!");
        }

    }

    /// <summary>
    /// Get background image component ref.
    /// </summary>
    /// <returns></returns>
    Image FindBackgroundComponent ()
    {
        return GameObject.Find("MatchBackground").GetComponent<Image>();
    }

    /// <summary>
    /// Finish match process; updated stats, set round text and set match state to finish the game.
    /// </summary>
    /// <param name="winnerPlayerName"></param>
    public void FinishMatch(string winnerPlayerName)
    {
        UpdateStatsText();
        SetRoundText(winnerPlayerName + " won the battle!");
        ShowRoundText(true);
        GameManager.lastWinnerName = winnerPlayerName;
        SetMatchState(MatchState.Finish);
    }

    /// <summary>
    /// Load the final scene after a pause of 4 seconds. It could be innecesary due the in game buttons and stats.
    /// </summary>
    /// <returns></returns>
    IEnumerator FinishedMatch ()
    {
        yield return new WaitForSeconds(4.0f);
        SceneManager.LoadScene("Final");
    }

    /// <summary>
    /// Set players behaviors refs. Preparing for a more than 2 players options.
    /// </summary>
    /// <returns></returns>
    int GetPlayersBehaviours ()
    {
        playersBehaviours = new PlayerBehaviour[2];
        playersBehaviours[0] = GameObject.FindGameObjectWithTag("Player1").GetComponent<PlayerBehaviour>();
        playersBehaviours[1] = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerBehaviour>();

        foreach (PlayerBehaviour pb in playersBehaviours)
        {
            pb.SetMatchManager(this);
        }

        return playersBehaviours.Length;
    }

    /// <summary>
    /// Set battle background with battleBackground property.
    /// </summary>
    void LoadMatchBackground ()
    {
        FindBackgroundComponent().sprite = battleBackground;
    }

    /// <summary>
    /// Show player dialog in its turn and; save for the answer (attacking) or check the answer (defending).
    /// </summary>
    /// <param name="player">Player who says something</param>
    /// <param name="iSentence">Index of dictionary sentence</param>
    public void PlayerSaySomething (Transform player, int iSentence)
    {
        foreach (PlayerBehaviour playerBehaviour in playersBehaviours)
        {
            playerBehaviour.ShowDialogText(playerBehaviour.tag == player.tag);
        }

        // Attack
        if (iLastSentenceUsed == -1)
        {
            iLastSentenceUsed = iSentence;
            StartCoroutine(EndTurnPauseSeconds(turnPauseTime, player.tag));
        }
        else // Defense
        {
            StartCoroutine(CheckRoundResult(iSentence));
        }
    }

    /// <summary>
    /// Randomize the player who starts the match.
    /// </summary>
    void RandomizePlayer ()
    {
        int rnd = Random.Range(1, 3);

        MatchState state = (rnd == 1) ? MatchState.Player1Turn : MatchState.Player2Turn;
        SetMatchState(state);
    }

    /// <summary>
    /// Set the current match dictionary with the json data loaded from a file.
    /// </summary>
    /// <param name="jData">Json data</param>
    void ReadInsultArray (string jData)
    {
        currentMatchDictionary = JsonHelper.FromJson<InsultAndAnswer>(jData);
    }

    /// <summary>
    /// The round winner player report its behaviour and sets the next round match state.
    /// </summary>
    /// <param name="winnerPlayerBehaviour"></param>
    public void ReceiveRoundWinnerResult (PlayerBehaviour winnerPlayerBehaviour)
    {
        // Assign next player to start    UGLY CODE
        if (winnerPlayerBehaviour.tag == "Player1")
        {
            SetMatchState(MatchState.Player1Turn);
        }
        else
        {
            SetMatchState(MatchState.Player2Turn);
        }
    }

    /// <summary>
    /// Sets match state and check it.
    /// </summary>
    /// <param name="ms">Which's the next match state?</param>
    public void SetMatchState(MatchState ms)
    {
        matchState = ms;

        CheckMatchState();
    }

    /// <summary>
    /// Set round result UI text.
    /// </summary>
    /// <param name="text">Round result.</param>
    void SetRoundText (string text)
    {
        if (!roundResultText)
        {
            roundResultText = GameObject.Find("RoundResultText").GetComponent<Text>();
        }
        roundResultText.text = text;
    }

    /// <summary>
    /// Show round result in game.
    /// </summary>
    /// <param name="show">Show it?</param>
    void ShowRoundText (bool show)
    {
        if (!roundResultText)
        {
            roundResultText = GameObject.Find("RoundResultText").GetComponent<Text>();
        }
        roundResultText.gameObject.SetActive(show);
    }

    /// <summary>
    /// Toogle round turn.
    /// </summary>
    void ToogleTurn ()
    {
        if (matchState == MatchState.Player1Turn)
        {
            SetMatchState(MatchState.Player2Turn);
        }
        else
        {
            SetMatchState(MatchState.Player1Turn);
        }
    }

    /// <summary>
    /// Update in game UI stats with current stats.
    /// </summary>
    void UpdateStatsText()
    {
        playersStatsTexts[0].text = playersBehaviours[0].StateToInt(playersBehaviours[0].playerMatchState).ToString();
        playersStatsTexts[1].text = playersBehaviours[1].StateToInt(playersBehaviours[1].playerMatchState).ToString();
    }

}
