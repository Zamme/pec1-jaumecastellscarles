# UOC – Máster en diseño y desarrollo de Videojuegos 2018
# Jaume Castells Carles
# PEC 1 - Un juego de aventuras

## Nombre
**Monkey Island Battles**

![Low Resolution video](https://gitlab.com/Zamme/pec1-jaumecastellscarles/blob/master/Docs/PEC1_JCC_low.mp4)

**_Original video at Docs/PEC1_JCC.mp4_** - https://gitlab.com/Zamme/pec1-jaumecastellscarles/blob/master/Docs/PEC1_JCC.mp4

## Descripción
Homenaje al videojuego **Monkey Island** (1990, Ron Gilbert and Tim Schafer) en forma de recreación de una de sus fases más conocidas; los combates de insultos en los bosques de Melee Island.

## Arte
Todo el arte es propiedad de Lucasfilm.

## Builds
Builds available:
### Windows
Path Builds/Win
### Linux
### Mac

## Programación
Toda la aplicación funciona con estados. Dependiendo del estado de cada objeto éste actúa con su propio comportamiento.

Hay tres conceptos básicos para su desarrollo:
* **Match** - una partida. Creará rounds nuevos hasta que un player gane 3 seguidos.  
* **Round** - consta de un ataque de un player y una defensa del otro player. El objetivo del juego es ganar 3 seguidos, ya sea escogiendo la respuesta correcta al insulto o no recibiendo la respuesta correcta al insulto lanzado.
* **Turn** - una frase. Un player lanza un insulto o una respuesta a un insulto dependiendo del estado de éste.
Los insultos y respuestas se cargan desde un archivo json alojado en StreamingAssets. Futuras versiones introduciran la posibilidad de cargar diferentes archivos.

### Clases
La aplicación consta de 9 clases.

#### GameManager
Clase estática encargada del desarrollo de toda la aplicación.

Actualmente (v1.0) sólo sirve para guardar el nombre del jugador ganador.

#### MatchManager
Clase encargada de crear una partida nueva (match) y desarrollarla.

Se comunica constantemente con las player behaviour instanciadas configurando su estado y recibiendo su información.

#### PlayerBehaviour
Clase abstracta encargada del funcionamiento de cada player, ya sea humano o virtual.

#### HumanBehaviour
Hereda casi todo el comportamiento de PlayerBehaviour.

Comportamiento humano para player.

Se diferencia de AIBehaviour principalmente en la interactividad para escoger opciones en juego.

#### AIBehaviour
Hereda casi todo el comportamiento de PlayerBehaviour.

Comportamiento npc para player.

Se diferencia de HumanBehaviour principalmente por la automatización de la elección de opciones.

#### InsultAndAnswer
Clase para guardar un insulto y una respuesta correcta.

Aunque podría hacerse como struct, he preferido hacerlo como clase por sus posibles futuros cambios.

#### JsonHelper
Clase para interacción con JsonUtilities para arrays.

#### FinalTextBehaviour
Clase asignada al texto final (escena final) que recoge el nombre del ganador de GameManager y lo muestra en pantalla.

**Mucha más información en el código de los scripts en forma de comentarios y sumarios.**

### Flujo
El flujo parte completamente del MatchManager.

#### Funciones del MatchManager
* Inicializa los estados de los players.
* Asigna estado de comportamiento a cada player por cada turno.
* Recibe las opciones escogidas por los players y calcula el resultado.
* Reporta a los players el resultado para que cada uno pueda contabilizar por su cuenta el estado de la partida.
* Empieza y termina las partidas.

## Para mejorar en futuras versiones
Hay muchos detalles que no he tenido tiempo de añadir y/o arreglar. Espero hacerlo en futuras versiones:  :-)
* No me convence el hecho de que los players tengan tanto control sobre sus victorias. Mejor sería guardarlo en el matchmanager para evitar hacks en multijugador online etc.
* Algunas partes (como pausas de tiempo) forman parte de otras funciones. Deberían estar totalmente a parte y reaprovecharlas. Por ejemplo, con una función para pausa con un parametro de tiempo y otro de acción posterior podría quedar mucho más optimizado.
* El player1 (Guybrush) no tiene animaciones incluidas. Hay que remediarlo.
* Hay 3 posiciones iniciales para cada player. La idea era que andaran hacia adelante y hacia atrás dependiendo del resultado de los rounds. La última posición coincidiría con un elemento de muerte (totalmente virtual, claro está) para mejorar la sensación tanto de derrota como de victoria. En futuras versiones...espero.
* Todo el arte es de Monkey Island (Lucasfilm copyright). Me gustaría adaptarlo a algo nuevo original.
* Faltan títulos, efectos (tanto gráficos como sonoros) y más personalización.
* No me gusta el responsive de la UI. A ver si en la próxima versión...
* Sé que «comportamiento» en inglés se escribe «behavior», però es que llevo años escribiéndolo mal y se ha convertido en una seña de identidad jijijiji. Perdón. ;-)
 